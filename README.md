**Warning: This can cause world corruption if applied to an already-generated world.**

World generation datapacks are still an experimental feature, and are not officially supported. This could break your world, freeze the game, or have other unintended side-effects. Use this datapack at your own risk.

---

This datapack halves the total height of the overworld. The maximum height is set to 160, and the minimum height is -32 (by default, these are 320 and -64).

* Biome/noise generation is tweaked to remain mostly similar to the default settings in these conditions. Large mountain peaks may seem "stubby", but most terrain is otherwise unchanged.

* The deepslate level is slightly higher to keep an approximate 50% ratio of "stone"/"deepslate" cave area.

* Ancient cities, which are normally at y=-50, have been moved up to compensate for the higher bedrock level.

### Benchmarking

To estimate the impact of this datapack on generation performance, I ran several tests on a server with [Chunky Pregenerator](https://dev.bukkit.org/projects/chunky-pregenerator) and [Paper](https://papermc.io) on default world settings.

The server had an initial memory of 1G and max memory of 6G.

I tested a generation distance of 1, 4, 7, 16, 22, 25, and 32 chunks with and without the datapack, taking the average of each test duration over three iterations. The graphs below show the chunk radius on the x-axis and seconds of generation time on the y-axis.

| Default World Generation | Small World Generation |
|---------|---------------|
| ![img](./.images/benchmark-default.png) | ![img](./.images/benchmark-small.png) |

*Note that, because it generates a square of chunks, the graph's x-scale is actually exponential in terms of number of chunks being generated.*

However, as chunks are normally generated individually, the smaller numbers on this graph are more relevant. As the quantity increases, chunk generation becomes more memory and i/o-bound, reducing the effects of this datapack (which will primarily benefit CPU-bound situations in addition to network/loading time).

The 1 chunk radius demonstrates a reduced average generation speed from 5 seconds to 1 second in this benchmark.


